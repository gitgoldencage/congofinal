package tn.esprit.investadvisor.management.client;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import tn.esprit.investadvisor.entities.Client;

/**
 * Session Bean implementation class GestionAddress
 */
@Stateless
public class ClientManagement implements ClientManagementRemote, ClientManagementLocal {

	@PersistenceContext(unitName = "investAdvisor")
	EntityManager entityManager;

	public ClientManagement() {
		super();
	}

	@Override
	public Boolean addClient(Client client) {
		try {
			entityManager.persist(client);
			return true;
		} catch (Exception e) {
			return false;
		}

	}

	@Override
	public Boolean updateClient(Client client) {

		try {
			entityManager.merge(client);
			return true;
		} catch (Exception e) {

			return false;

		}
	}

	@Override
	public Boolean deleteClient(Client client) {

		try {
			entityManager.remove(entityManager.merge(client));
			return true;
		} catch (Exception e) {

			return false;

		}
	}

	@Override
	public Client findById(Integer idclient) {

		Client client = null;
		try {
			client = entityManager.find(Client.class, idclient);

		} catch (Exception e) {

		}
		return client;

	}

	@Override
	public List<Client> findAllClients() {

		Query query = entityManager.createQuery("select c from Client c");
		return query.getResultList();
	}

}
