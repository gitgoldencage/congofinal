package tn.esprit.investadvisor.management.admin;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.investadvisor.entities.Admin;

@Remote
public interface AdminManagementRemote {
	Boolean addAdmin(Admin admin);
    Boolean updateAdmin(Admin admin);
    Boolean deleteAdmin(Admin admin);
    Admin findAdminById(Integer idAdmin);
    List<Admin> findAllAdmin();
}
