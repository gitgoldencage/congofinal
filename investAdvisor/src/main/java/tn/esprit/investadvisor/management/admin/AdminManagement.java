package tn.esprit.investadvisor.management.admin;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import tn.esprit.investadvisor.entities.Admin;

/**
 * Session Bean implementation class AdminManagement
 */
@Stateless
public class AdminManagement implements AdminManagementRemote, AdminManagementLocal {
@PersistenceContext(unitName="investAdvisor")
	EntityManager entityManager;
    public AdminManagement() {
        super();
    }

	@Override
	public Boolean addAdmin(Admin admin) {
		try {
			entityManager.persist(admin);
			return true;
			
			} catch (Exception e) {
			
		}
		return false;
	}

	@Override
	public Boolean updateAdmin(Admin admin) {
	
		try {
			entityManager.merge(admin);
			return true;
			
			} catch (Exception e) {
			
		}
		return false;
	}

	@Override
	public Boolean deleteAdmin(Admin admin) {
		try {
			entityManager.remove(entityManager.merge(admin));
			return true;
			
			} catch (Exception e) {
			
		}
		return false;
	}

	@Override
	public Admin findAdminById(Integer idAdmin) {
		Admin admin=null;
		try {
			admin=entityManager.find(Admin.class, idAdmin);
		
			
			} catch (Exception e) {
			
		}
		return admin;
	}

	@Override
	public List<Admin> findAllAdmin() {
Query query=entityManager.createQuery("select a from Admin a");
return query.getResultList();
	}

}
