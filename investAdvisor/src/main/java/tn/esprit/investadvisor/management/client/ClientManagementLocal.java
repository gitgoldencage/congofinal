package tn.esprit.investadvisor.management.client;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.investadvisor.entities.Client;

@Local
public interface ClientManagementLocal {
	  Boolean addClient(Client client);
		Boolean updateClient(Client client);
		Boolean deleteClient(Client client);
		Client findById(Integer idclient);
		List<Client> findAllClients();


}
