package tn.esprit.investadvisor.management.admin;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.investadvisor.entities.Admin;

@Local
public interface AdminManagementLocal {
	Boolean addAdmin(Admin admin);
    Boolean updateAdmin(Admin admin);
    Boolean deleteAdmin(Admin admin);
    Admin findAdminById(Integer idAdmin);
    List<Admin> findAllAdmin();

}
