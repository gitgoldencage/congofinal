package tn.esprit.investadvisor.management.client;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.investadvisor.entities.Client;


@Remote
public interface ClientManagementRemote {
    Boolean addClient(Client client);
	Boolean updateClient(Client client);
	Boolean deleteClient(Client client);
	Client findById(Integer idclient);
	List<Client> findAllClients();

}
