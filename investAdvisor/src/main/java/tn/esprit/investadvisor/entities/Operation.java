package tn.esprit.investadvisor.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Entity implementation class for Entity: Operation
 *
 */
@Entity
@Table(name = "t_operation")
public class Operation implements Serializable {

	private OperationPk operationPk;
	private Integer quantity;
	private Date date;
	private Float transactionFee;
	private State state;
	private Wallet wallet;
	private Securities securities;
	private static final long serialVersionUID = 1L;
	
	@ManyToOne
	@JoinColumn(name="wallet_id",referencedColumnName="id",insertable=false,updatable=false)
	public Wallet getWallet() {
		return wallet;
	}
	public Operation(Integer quantity, Date date, Float transactionFee,
			State state, Wallet wallet, Securities securities) {
		super();
		this.quantity = quantity;
		this.date = date;
		this.transactionFee = transactionFee;
		this.state = state;
		this.wallet = wallet;
		this.securities = securities;
		this.operationPk=new OperationPk(securities.getId(),wallet.getId());
	}
	public void setWallet(Wallet wallet) {
		this.wallet = wallet;
	}
	@ManyToOne
	@JoinColumn(name="securities_id",referencedColumnName="id",insertable=false,updatable=false)
	public Securities getSecurities() {
		return securities;
	}
	public void setSecurities(Securities securities) {
		this.securities = securities;
	}
	
	
	public Operation() {
		super();
	}
	@EmbeddedId
	public OperationPk getOperationPk() {
		return operationPk;
	}

	public void setOperationPk(OperationPk operationPk) {
		this.operationPk = operationPk;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Float getTransactionFee() {
		return transactionFee;
	}

	public void setTransactionFee(Float transactionFee) {
		this.transactionFee = transactionFee;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

}
