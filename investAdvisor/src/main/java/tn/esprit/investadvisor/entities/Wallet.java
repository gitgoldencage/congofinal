package tn.esprit.investadvisor.entities;


import java.io.Serializable;
import java.lang.Float;
import java.lang.Integer;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Wallet
 *
 */
@Entity
@Table(name="t_wallet")
public class Wallet implements Serializable {

	
	private Integer id;
	private Float walletValue;
	private static final long serialVersionUID = 1L;
	private Client client;
	private List<Operation> operations;
	private List<Securities> securities; 

	public Wallet() {
		super();
	}   
	@Id    
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}   
	public Float getWalletValue() {
		return this.walletValue;
	}

	public void setWalletValue(Float walletValue) {
		this.walletValue = walletValue;
	}   
	/*public Integer getValid() {
		return this.valid;
	}

	public void setValid(Integer valid) {
		this.valid = valid;
	}*/

	@OneToOne
	public Client getClient() {
		return client;
	}
	public void setClient(Client client) {
		this.client = client;
	}
	@OneToMany(mappedBy="wallet")
	public List<Operation> getOperations() {
		return operations;
	}
	public void setOperations(List<Operation> operations) {
		this.operations = operations;
	}
	@ManyToMany
	public List<Securities> getSecurities() {
		return securities;
	}
	public void setSecurities(List<Securities> securities) {
		this.securities = securities;
	}
	
}
