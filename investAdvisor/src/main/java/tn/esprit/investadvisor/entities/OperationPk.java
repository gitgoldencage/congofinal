package tn.esprit.investadvisor.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
@Embeddable
public class OperationPk implements Serializable{

	/**
	 * 
	 */
	private Integer walletId;
	private Integer securitiesId;
	@Column(name="wallet_id")
	public Integer getWalletId() {
		return walletId;
	}
	public void setWalletId(Integer walletId) {
		this.walletId = walletId;
	}
	@Column(name="securities_id")
	public Integer getSecuritiesId() {
		return securitiesId;
	}
	public void setSecuritiesId(Integer securitiesId) {
		this.securitiesId = securitiesId;
	}
	private static final long serialVersionUID = 1L;
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((securitiesId == null) ? 0 : securitiesId.hashCode());
		result = prime * result
				+ ((walletId == null) ? 0 : walletId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OperationPk other = (OperationPk) obj;
		if (securitiesId == null) {
			if (other.securitiesId != null)
				return false;
		} else if (!securitiesId.equals(other.securitiesId))
			return false;
		if (walletId == null) {
			if (other.walletId != null)
				return false;
		} else if (!walletId.equals(other.walletId))
			return false;
		return true;
	}
	public OperationPk(Integer walletId, Integer securitiesId) {
		super();
		this.walletId = walletId;
		this.securitiesId = securitiesId;
	}
	public OperationPk() {
		super();
		// TODO Auto-generated constructor stub
	}
	
}
