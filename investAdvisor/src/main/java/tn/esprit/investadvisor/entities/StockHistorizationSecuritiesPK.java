package tn.esprit.investadvisor.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class StockHistorizationSecuritiesPK implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer stockHistorizationId;
	private Integer securitiesId;
	
	public StockHistorizationSecuritiesPK() {
		super();
	}
	
	@Column(name="stock_historization_id")
	public Integer getStockHistorizationId() {
		return stockHistorizationId;
	}
	public void setStockHistorizationId(Integer stockHistorizationId) {
		this.stockHistorizationId = stockHistorizationId;
	}
	@Column(name="securities_id")
	public Integer getSecuritiesId() {
		return securitiesId;
	}
	public void setSecuritiesId(Integer securitiesId) {
		this.securitiesId = securitiesId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((securitiesId == null) ? 0 : securitiesId.hashCode());
		result = prime
				* result
				+ ((stockHistorizationId == null) ? 0 : stockHistorizationId
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StockHistorizationSecuritiesPK other = (StockHistorizationSecuritiesPK) obj;
		if (securitiesId == null) {
			if (other.securitiesId != null)
				return false;
		} else if (!securitiesId.equals(other.securitiesId))
			return false;
		if (stockHistorizationId == null) {
			if (other.stockHistorizationId != null)
				return false;
		} else if (!stockHistorizationId.equals(other.stockHistorizationId))
			return false;
		return true;
	}

	public StockHistorizationSecuritiesPK(Integer stockHistorizationId,
			Integer securitiesId) {
		super();
		this.stockHistorizationId = stockHistorizationId;
		this.securitiesId = securitiesId;
	}
	
}
