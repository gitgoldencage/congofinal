package tn.esprit.investadvisor.entities;

import java.io.Serializable;
import java.lang.Integer;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: StockHistorization
 *
 */
@Entity
@Table(name="t_stockHistorization")
public class StockHistorization implements Serializable {

	
	private Integer id;
	private Date date;
	private static final long serialVersionUID = 1L;
	private  List<StockHistorizationSecurities>  stockHistorizationSecurities;
	

	public StockHistorization() {
		super();
	}   
	@Id    
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}   
	
	@Temporal(TemporalType.DATE)
	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	@OneToMany(mappedBy="stockHistorization")
	public List<StockHistorizationSecurities> getStockHistorizationSecurities() {
		return stockHistorizationSecurities;
	}
	public void setStockHistorizationSecurities(
			List<StockHistorizationSecurities> stockHistorizationSecurities) {
		this.stockHistorizationSecurities = stockHistorizationSecurities;
	}
   
}
