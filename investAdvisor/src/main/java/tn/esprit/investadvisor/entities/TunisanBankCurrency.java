package tn.esprit.investadvisor.entities;

import java.io.Serializable;
import javax.persistence.*;
import tn.esprit.investadvisor.entities.Currency;

/**
 * Entity implementation class for Entity: TunisanBankCurrency
 *
 */
@Entity
@DiscriminatorValue("tunisian_bank_currency")
public class TunisanBankCurrency extends Currency implements Serializable {

	
	private static final long serialVersionUID = 1L;
	private BankCompany bankCompany;
	
	public TunisanBankCurrency() {
		super();
	}
	@ManyToOne
	@JoinColumn(name="bank_company")
	public BankCompany getBankCompany() {
		return bankCompany;
	}

	public void setBankCompany(BankCompany bankCompany) {
		this.bankCompany = bankCompany;
	}
   
}
