package tn.esprit.investadvisor.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

import tn.esprit.investadvisor.entities.Currency;

/**
 * Entity implementation class for Entity: CentralBankCurrency
 *
 */
@Entity
@DiscriminatorValue("Central Bank Currency")
public class CentralBankCurrency extends Currency implements Serializable {

	
	private static final long serialVersionUID = 1L;
	private List<CurrencyHistorization> currencyHistorizations;
	
	@OneToMany(mappedBy="centralBankCurrency")
	public List<CurrencyHistorization> getCurrencyHistorizations() {
		return currencyHistorizations;
	}

	public void setCurrencyHistorizations(
			List<CurrencyHistorization> currencyHistorizations) {
		this.currencyHistorizations = currencyHistorizations;
	}

	public CentralBankCurrency() {
		super();
	}
	

   
}
