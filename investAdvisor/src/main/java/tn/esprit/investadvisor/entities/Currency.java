package tn.esprit.investadvisor.entities;

import java.io.Serializable;
import java.lang.Float;
import java.lang.Integer;
import java.lang.String;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Curency
 *
 */
@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(
		name="currency_source",
		discriminatorType=DiscriminatorType.STRING)
@Table(name="t_currency")
public class Currency implements Serializable {
	
	private Integer id;
	private Float offer;
	private String name;
	private String logo;
	private static final long serialVersionUID = 1L;
	private Securities securities;

	public Currency() {
		super();
	}   
	@Id    
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}   
	public Float getOffer() {
		return this.offer;
	}

	public void setOffer(Float offer) {
		this.offer = offer;
	}   
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}   
	public String getLogo() {
		return this.logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}
	@ManyToOne
	@JoinColumn(name="securities")
	public Securities getSecurities() {
		return securities;
	}
	public void setSecurities(Securities securities) {
		this.securities = securities;
	}
   
}
