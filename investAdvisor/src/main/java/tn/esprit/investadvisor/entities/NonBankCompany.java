package tn.esprit.investadvisor.entities;

import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Entity implementation class for Entity: NonBankCompany
 *
 */
@Entity
@DiscriminatorValue("Non Bank Company")
public class NonBankCompany extends Company implements Serializable {

	
	private static final long serialVersionUID = 1L;

	public NonBankCompany() {
		super();
	}
   
}
