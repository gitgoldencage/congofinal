package tn.esprit.investadvisor.entities;

public enum State {
	OnSell,
	Sold,
	Bought

}
